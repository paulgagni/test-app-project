# src/api/ping.py


from flask import Blueprint
from flask_restx import Resource, Api

# create a new instance of the Blueprint class and bounded the Ping resource to it
ping_blueprint = Blueprint('ping', __name__)
api = Api(ping_blueprint)


class Ping(Resource):
    def get(self):
        return {
            'status': 'success',
            'message': 'pong!'
        }


api.add_resource(Ping, '/ping')